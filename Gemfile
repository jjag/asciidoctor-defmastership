# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

source('https://rubygems.org')

gemspec

ruby RUBY_VERSION

group :development do
  # cucumber steps for command line tests
  gem 'aruba',                 '~> 2.3'
  # bdd
  gem 'cucumber',              '~> 9.2'

  # mutation testing
  plan = 'oss'
  key = '7oac4dMz95cTUuFPtGDfTDSQep6ZhdGW'
  source "https://#{plan}:#{key}@gem.mutant.dev" do
    # license needed
    gem 'mutant-license', '~> 0.1'
  end
  # mutation testing
  gem 'mutant-rspec', '~> 0.12'
  # to parse provided Rakefile
  gem 'rake', '~> 13.2'
  # needed by yard to render documentation
  gem 'rdoc', '~> 6.12'
  # tdd
  gem 'rspec', '~> 3.13'
  # code need to be clean
  gem 'rubocop', '1.72'
  # code need to be clean
  gem 'rubocop-performance', '~> 1.23'
  # Rakile needs to be clean
  gem 'rubocop-rake', '~> 0.6'
  # unit tests need to be clean
  gem 'rubocop-rspec', '~> 3.4'
  # detect selling code
  gem 'reek', '~> 6.4'
  # What is tdd without code coverage ?
  gem 'simplecov', '~> 0.22'
  # to document code
  gem 'yard', '~> 0.9'
end

group :debugging do
  # Sometimes, we need to debug
  gem 'pry', '~> 0.15'
end
