# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

# :nocov:
require('asciidoctor/extensions') unless RUBY_ENGINE == 'opal'
# :nocov:

# An extension that allow to define applicable definitions
#
# Usage
#   :tag-mylabel-color: yellow
#   :tag-otherlabel-color: red
#
#
#   [define, requirement, TOTO-0001, a summary, [mylabel, otherlabel]]
#   --
#   The system shall allow to do lots of things.
#   --
#
# or
#
#   [define, requirement, TOTO-0001]
#   This shall be nice.
#
require('asciidoctor-defmastership/regexp_dispatcher')
require('defmastership/core/constants')
require('defmastership/core/parsing_state')

module Asciidoctor
  # Module to host Defmastership preprocessor
  module Defmastership
    # Preprocessor to replace adoc statements
    # This class smells of :reek:InstanceVariableAssumption.
    # Not an issue because process is the only method used by Asciidoctor
    class Preprocessor < Asciidoctor::Extensions::Preprocessor
      REGEXPS = {
        eref_config: ::Defmastership::Core::DMRegexp::EREF_CONFIG,
        definition: ::Defmastership::Core::DMRegexp::DEFINITION,
        eref_def: ::Defmastership::Core::DMRegexp::EREF_DEF,
        iref_def: ::Defmastership::Core::DMRegexp::IREF_DEF,
        attr_set: ::Defmastership::Core::DMRegexp::ATTR_SET,
        variable_def: ::Defmastership::Core::DMRegexp::VARIABLE_DEF
      }.freeze

      private_constant :REGEXPS

      # @param _config [Hash] configuration Hash for this preprocessor instance
      def initialize(_config = {})
        super
        @has_url = Set.new
        @variables = {}
        @parsing_state = ::Defmastership::Core::ParsingState.new
      end

      # Entry point for the plugin call
      #
      # @param _document [Asciidoctor::Document] the current document
      # @param reader [Asciidoctor::Reader] to retrieve lines from AsciiDoc source file
      # This method smells of :reek:FeatureEnvy
      def process(_document, reader)
        return reader if reader.eof?

        reader.unshift_lines(parse_and_replace(reader.read_lines))
      end

      # Process a definition line
      #
      # @param _line [String] the original line
      # @param match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      def build_definition(_line, match)
        Helper::DefinitionStringBuilder.new(match, show_explicit_checksum(match)).str_a
      end

      # Process a variable setting line
      #
      # @param line [String] the original line
      # @param match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      def variable_set(line, match)
        @variables.merge!(Helper.variable_hash(match))
        [line]
      end

      # Process a set url line
      #
      # @param line [String] the original line
      # @param match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      def set_eref_url_if_any(line, match)
        @has_url.add(match[:reference]) if Helper.valid_eref_url?(match)
        [line]
      end

      # Process an external ref line
      #
      # @param _line [String] the original line
      # @param match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      def build_external_ref(_line, match)
        return [] unless show_ext_ref(match)

        extrefs = match[:extrefs].split(/\s*,\s*/)
        extref_line = extrefs.map { |ref| build_link(ref, match) }
        ["[.external_reference]\#{eref-#{match[:reference]}-prefix} #{extref_line.join(', ')}.#"]
      end

      # Process an internal ref line
      #
      # @param line [String] the original line
      # @param _match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      # This method smells of :reek:UtilityFunction
      def build_internal_ref(line, _match)
        [
          line.gsub(REGEXPS.fetch(:iref_def)) do
            intref = Regexp.last_match[:intref]
            "<<#{intref},#{intref}>>"
          end
        ]
      end

      # Process an attribute setting line
      #
      # @param _line [String] the original line
      # @param match [MatchData] the match data from the matching Regexp
      # @return [Array<String>] the lines to replace the original line
      # This method smells of :reek:UtilityFunction
      def attribute_setting(_line, match)
        attr_name = match[:attr]
        [
          '[.attribute]',
          "[.attribute_prefix.attribute_#{attr_name}_prefix]\#{attr-#{attr_name}-prefix}# " \
          "[.attribute_value.attribute_#{attr_name}_value]##{match[:value]}#."
        ]
      end

      private

      def build_regexp_dispatcher
        regexp_dispatcher = RegexpDispatcher.new(self)
        regexp_dispatcher
          .add_rule(REGEXPS.fetch(:eref_config), :set_eref_url_if_any)
          .add_rule(REGEXPS.fetch(:definition), :build_definition)
          .add_rule(REGEXPS.fetch(:eref_def), :build_external_ref)
          .add_rule(REGEXPS.fetch(:iref_def), :build_internal_ref)
          .add_rule(REGEXPS.fetch(:attr_set), :attribute_setting)
          .add_rule(REGEXPS.fetch(:variable_def), :variable_set)
      end

      def parse_and_replace(lines)
        regexp_dispatcher = build_regexp_dispatcher
        lines.reduce([]) do |new_lines, line|
          next new_lines + [line] unless @parsing_state.enabled?(line)

          next new_lines + regexp_dispatcher.replace(line)
        end
      end

      def show_explicit_checksum(match)
        match[:explicit_checksum] &&
          !@variables['show-explicit-checksum'].eql?('disable') &&
          !@variables["show-#{match[:type]}-explicit-checksum"].eql?('disable')
      end

      def build_link(ref, match)
        refname = match[:reference]
        return ref unless @has_url.include?(refname)

        ref_pattern =
          @variables["eref-#{refname}-ref-pattern"] || '#%s'
        ref_str = format(ref_pattern, ref)

        "link:{eref-#{refname}-url}#{ref_str}[#{ref}]"
      end

      def show_ext_ref(match)
        !@variables['show-ext-ref'].eql?('disable') &&
          !@variables["show-#{match[:reference]}-ext-ref"].eql?('disable')
      end
    end

    # Proepocessors class Helpers
    class Preprocessor
      # Helpers for Preprocessor class
      module Helper
        # Isolate the definition macro
        class DefinitionStringBuilder
          # @param match [MatchData] match data from defintion Regexp
          # @param show_explicit_checksum [Boolean] sepcify if we need to show checksums in rendered document
          def initialize(match, show_explicit_checksum)
            @match = match
            @show_explicit_checksum = show_explicit_checksum
          end

          # @return [Array<String>] the lines to replace defininition's line
          def str_a
            explicit_checksum_str = " [.checksum]#(#{@match[:explicit_checksum]})#" if @show_explicit_checksum
            type = @match[:type]

            build_str_a(
              @match[:reference],
              type,
              "#{Helper.explicit_version_str(@match[:explicit_version])}#{explicit_checksum_str}" \
              "#{Helper.summary_str(@match[:summary], type)}" \
              "#{Helper.labels_str(@match[:labels])}"
            )
          end

          private

          def build_str_a(reference, type, reference_decoration)
            [
              ".#{reference}#{reference_decoration}",
              "[##{reference}.define.#{type}]"
            ]
          end
        end

        # @param match [MatchData] match data from external ref setting Regexp
        # @return [Boolean] true if the match data includes a valid URL
        def self.valid_eref_url?(match)
          match[:symb] == 'url' && !match[:value].eql?('none')
        end

        # @param summary [String] summary of a definition
        # @return [String] the replacement string for summary
        def self.summary_str(summary, type)
          return unless summary

          " [.def-summary.def-#{type}-summary]##{summary}#"
        end

        # @param labels [Array[String]] List of labels of a definition
        # @return [String] the replacement string for labels
        def self.labels_str(labels)
          return unless labels

          labels_strings =
            labels.split(/\s*,\s*/).reduce([]) do |acc, label|
              acc << "[.tag.{tag-#{label}-color}]##{label}#"
            end
          " #{labels_strings.join(' ')}"
        end

        # @param explicit_version [String] an explicit version setting
        # @return [String] the replacement string for explicit version
        def self.explicit_version_str(explicit_version)
          " [.version]#(#{explicit_version})#" if explicit_version
        end

        # @param match [MatchData] the match data from the matching variable setting Regexp
        # @return [[Hash{String => Object}]] the hash corresponding to the variable setting
        def self.variable_hash(match)
          { match[:varname] => match[:value] }
        end
      end
    end
  end
end
