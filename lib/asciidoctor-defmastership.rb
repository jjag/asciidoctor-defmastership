# rubocop:disable Naming/FileName
# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

if RUBY_ENGINE == 'opal'
  require('asciidoctor-defmastership/preprocessor')
else
  require_relative('asciidoctor-defmastership/preprocessor')
end

Asciidoctor::Extensions.register do
  preprocessor Asciidoctor::Defmastership::Preprocessor
end

# rubocop:enable Naming/FileName
