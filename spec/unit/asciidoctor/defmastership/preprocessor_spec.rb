# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

RSpec.describe(Asciidoctor::Defmastership::Preprocessor) do
  describe '.new' do
    subject(:preprocessor) { described_class.new({ whatever_option: :whatever_value }) }

    it { expect(described_class).to(be < Asciidoctor::Extensions::Preprocessor) }
    it { is_expected.not_to(be_nil) }
    it { expect(preprocessor.config).to(include({ whatever_option: :whatever_value })) }
  end

  describe '#process' do
    let(:reader)   { instance_double(Asciidoctor::Reader, 'reader') }
    let(:document) { nil                                            }

    context 'when empty document' do
      before do
        allow(reader).to(receive(:eof?).and_return(true))
        described_class.new.process(document, reader)
      end

      it { expect(reader).to(have_received(:eof?)) }

      it { expect(described_class.new.process(document, reader)).to(be(reader)) }
    end

    first_time = true
    [
      {
        desc: 'document with definition',
        input: ['[define, whatever, TOTO-0001]'],
        output: [
          '.TOTO-0001',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with explicit checksum',
        input: ['[define, whatever, TOTO-0001(~12ab)]'],
        output: [
          '.TOTO-0001 [.checksum]#(~12ab)#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with explicit checksum (anabled)',
        input: [
          ':show-whatever-explicit-checksum: enable',
          ':show-explicit-checksum: enable',
          '[define, whatever, TOTO-0001(~12ab)]'
        ],
        output: [
          ':show-whatever-explicit-checksum: enable',
          ':show-explicit-checksum: enable',
          '.TOTO-0001 [.checksum]#(~12ab)#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with not shown explicit checksum ',
        input: [
          ':show-explicit-checksum: disable',
          '[define, whatever, TOTO-0001(~12ab)]'
        ],
        output: [
          ':show-explicit-checksum: disable',
          '.TOTO-0001',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with not shown (2) explicit checksum ',
        input: [
          ':show-whatever-explicit-checksum: disable',
          '[define, whatever, TOTO-0001(~12ab)]',
          '[define, other_type, TOTO-0002(~12abcdef)]'
        ],
        output: [
          ':show-whatever-explicit-checksum: disable',
          '.TOTO-0001',
          '[#TOTO-0001.define.whatever]',
          '.TOTO-0002 [.checksum]#(~12abcdef)#',
          '[#TOTO-0002.define.other_type]'
        ]
      },
      {
        desc: 'definition with explicit version',
        input: ['[define, whatever, TOTO-0001(abcd)]'],
        output: [
          '.TOTO-0001 [.version]#(abcd)#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with explicit checksum and explicit version',
        input: ['[define, whatever, TOTO-0001(abcd~12ab)]'],
        output: [
          '.TOTO-0001 [.version]#(abcd)# [.checksum]#(~12ab)#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with not shown explicit checksum and explicit version',
        input: [
          ':show-explicit-checksum: disable',
          '[define, whatever, TOTO-0001(abcd~12ab)]'
        ],
        output: [
          ':show-explicit-checksum: disable',
          '.TOTO-0001 [.version]#(abcd)#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with tag',
        input: ['[define, whatever, TOTO-0001, [atag]]'],
        output: [
          '.TOTO-0001 [.tag.{tag-atag-color}]#atag#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with multiple tags',
        input: ['[define, whatever, TOTO-0001, [   atag   ,     btag,ctag   ]]'],
        output: [
          '.TOTO-0001 [.tag.{tag-atag-color}]#atag# [.tag.{tag-btag-color}]#btag# [.tag.{tag-ctag-color}]#ctag#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with summary',
        input: ['[define, whatever, TOTO-0001, a summary]'],
        output: [
          '.TOTO-0001 [.def-summary.def-whatever-summary]#a summary#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with summary including comma',
        input: ['[define, whatever, TOTO-0001, " a, summary "]'],
        output: [
          '.TOTO-0001 [.def-summary.def-whatever-summary]#a, summary#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with summary and tag',
        input: ['[define, whatever, TOTO-0001, a summary, [atag]]'],
        output: [
          '.TOTO-0001 [.def-summary.def-whatever-summary]#a summary# [.tag.{tag-atag-color}]#atag#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with white spaces',
        input: ['  [  define   ,   whatever  ,TOTO-0001  ,[  atag  ]  ]   '],
        output: [
          '.TOTO-0001 [.tag.{tag-atag-color}]#atag#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'definition with NO white spaces',
        input: ['[define,whatever,TOTO-0001,[atag]]'],
        output: [
          '.TOTO-0001 [.tag.{tag-atag-color}]#atag#',
          '[#TOTO-0001.define.whatever]'
        ]
      },
      {
        desc: 'comments',
        input: ['// [define, whatever, TOTO-0001, [atag]]'],
        output: ['// [define, whatever, TOTO-0001, [atag]]']
      },
      {
        desc: 'included in ....',
        input: [
          '....',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '....',
          '[define, whatever, TOTO-0002]'
        ],
        output: [
          '....',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '....',
          '.TOTO-0002',
          '[#TOTO-0002.define.whatever]'
        ]
      },
      {
        desc: 'included in ----',
        input: [
          '----',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '----',
          '[define, whatever, TOTO-0002]'
        ],
        output: [
          '----',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '----',
          '.TOTO-0002',
          '[#TOTO-0002.define.whatever]'
        ]
      },
      {
        desc: 'included in ---- and ....',
        input: [
          '----',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '....',
          '[define, whatever, TOTO-0002]',
          '....',
          '----'
        ],
        output: [
          '----',
          '[define, whatever, TOTO-0001]',
          'blabla',
          '....',
          '[define, whatever, TOTO-0002]',
          '....',
          '----'
        ]
      },
      {
        desc: 'external ref definition',
        input: [
          ':eref-implements-url: whatever',
          'defs:eref[implements, [SYSTEM-0012]]'
        ],
        output: [
          ':eref-implements-url: whatever',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.external_reference]#{eref-implements-prefix} ' \
          'link:{eref-implements-url}#SYSTEM-0012[SYSTEM-0012].#'
          # rubocop:enable Lint/InterpolationCheck
        ]
      },
      {
        desc: 'external ref definition with ref pattern',
        input: [
          ':eref-implements-url: whatever',
          ':eref-implements-ref-pattern: ---%s---',
          'defs:eref[implements, [SYSTEM-0012]]'
        ],
        output: [
          ':eref-implements-url: whatever',
          ':eref-implements-ref-pattern: ---%s---',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.external_reference]#{eref-implements-prefix} ' \
          'link:{eref-implements-url}---SYSTEM-0012---[SYSTEM-0012].#'
          # rubocop:enable Lint/InterpolationCheck
        ]
      },
      {
        desc: 'external ref definition without url',
        input: [
          ':eref-implements-prefix: whatever',
          'defs:eref[implements, [SYSTEM-0012]]'
        ],
        output: [
          ':eref-implements-prefix: whatever',
          "[.external_reference]\#{eref-implements-prefix} SYSTEM-0012.#"
        ]
      },
      {
        desc: 'when external ref definition without url=none',
        input: [
          ':eref-implements-url: none',
          'defs:eref[implements, [SYSTEM-0012]]'
        ],
        output: [
          ':eref-implements-url: none',
          "[.external_reference]\#{eref-implements-prefix} SYSTEM-0012.#"
        ]
      },
      {
        desc: 'multiple external ref',
        input: [
          ':eref-implements-url: whatever',
          'defs:eref[implements, [SYSTEM-0012,SYSTEM-0013]]'
        ],
        output: [
          ':eref-implements-url: whatever',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.external_reference]#{eref-implements-prefix} ' \
          'link:{eref-implements-url}#SYSTEM-0012[SYSTEM-0012], ' \
          'link:{eref-implements-url}#SYSTEM-0013[SYSTEM-0013].#'
          # rubocop:enable Lint/InterpolationCheck
        ]
      },
      {
        desc: 'multiple external ref (spacing changes)',
        input: [
          ':eref-implements-url: whatever',
          'defs:eref[implements, [    SYSTEM-0012   ,    SYSTEM-0013    ]]'
        ],
        output: [
          ':eref-implements-url: whatever',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.external_reference]#{eref-implements-prefix} ' \
          'link:{eref-implements-url}#SYSTEM-0012[SYSTEM-0012], ' \
          'link:{eref-implements-url}#SYSTEM-0013[SYSTEM-0013].#'
          # rubocop:enable Lint/InterpolationCheck
        ]
      },
      {
        desc: 'multiple external ref without url',
        input: [
          ':show-ext-ref: enable',
          ':show-implements-ext-ref: enable',
          'defs:eref[implements, [SYSTEM-0012, SYSTEM-0013]]'
        ],
        output: [
          ':show-ext-ref: enable',
          ':show-implements-ext-ref: enable',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.external_reference]#{eref-implements-prefix} ' \
          'SYSTEM-0012, SYSTEM-0013.#'
          # rubocop:enable Lint/InterpolationCheck
        ]
      },
      {
        desc: 'some external ref not shown',
        input: [
          ':show-ext-ref: enable',
          ':show-implements-ext-ref: disable',
          'defs:eref[implements, [SYSTEM-0012, SYSTEM-0013]]'
        ],
        output: [
          ':show-ext-ref: enable',
          ':show-implements-ext-ref: disable'
        ]
      },
      {
        desc: 'all external ref not shown',
        input: [
          ':show-ext-ref: disable',
          'defs:eref[implements, [SYSTEM-0012, SYSTEM-0013]]'
        ],
        output: [':show-ext-ref: disable']
      },
      {
        desc: 'internal ref',
        input: ['whatever before defs:iref[TOTO-0012] whatever afer'],
        output: ['whatever before <<TOTO-0012,TOTO-0012>> whatever afer']
      },
      {
        desc: 'multiple internal ref',
        input: ['defs:iref[TOTO-0012] whatever defs:iref[TOTO-0014]'],
        output: ['<<TOTO-0012,TOTO-0012>> whatever <<TOTO-0014,TOTO-0014>>']
      },
      {
        desc: 'attributes',
        input: ['defs:attribute[verifiedby, Test]'],
        output: [
          '[.attribute]',
          # ignore rubocop warning: this is not ruby interpolation
          # rubocop:disable Lint/InterpolationCheck
          '[.attribute_prefix.attribute_verifiedby_prefix]#{attr-verifiedby-prefix}# ' \
          '[.attribute_value.attribute_verifiedby_value]#Test#.'
          # rubocop:enable Lint/InterpolationCheck
        ]
      }
    ].each do |entry|
      describe "when #{entry[:desc]}" do
        before do
          allow(reader).to(
            receive_messages(
              eof?: false,
              read_lines: entry[:input]
            )
          )
          allow(reader).to(receive(:unshift_lines))
          described_class.new.process(document, reader)
        end

        it { expect(reader).to(have_received(:read_lines)) } if first_time
        it { expect(reader).to(have_received(:unshift_lines).with(entry[:output])) }

        first_time = false
      end
    end
  end
end
