# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

class Effective
  def foo(_, _); end
  def bar(_, _); end
end

RSpec.describe(Asciidoctor::Defmastership::RegexpDispatcher) do
  subject(:subs) { described_class.new(effective_subs) }

  let(:effective_subs) { instance_double(Effective) }

  describe '.new' do
    it { is_expected.not_to(be_nil) }
  end

  describe '.add_rule' do
    it 'allows to chaine the call' do
      expect(subs.add_rule(/^foo$/, :foo)).to(eq(subs))
    end
  end

  describe '.replace' do
    before do
      allow(effective_subs).to(receive(:foo)).and_return(['oof'])
      subs.add_rule(/^foo$/, :foo)
    end

    context 'when matching the only one rule' do
      it 'calls effective object method' do
        subs.replace('foo')
        expect(effective_subs).to(have_received(:foo).with('foo', /^foo$/.match('foo')))
      end

      it 'returns effective object method call return' do
        expect(subs.replace('foo')).to(eq(['oof']))
      end
    end

    context 'when matching no rule' do
      it 'does not call effective object method' do
        subs.replace('bar')
        expect(effective_subs).not_to(have_received(:foo))
      end

      it 'returns original line' do
        expect(subs.replace('bar')).to(eq(['bar']))
      end
    end

    context 'when matching one rule over multiple rules' do
      before do
        allow(effective_subs).to(receive(:bar)).and_return(['rab'])
        subs.add_rule(/bar/, :bar)
      end

      it 'calls effective object method on matching line' do
        subs.replace('bar')
        expect(effective_subs).to(have_received(:bar))
      end

      it 'does not call other effective object method' do
        subs.replace('bar')
        expect(effective_subs).not_to(have_received(:foo))
      end

      it 'returns effective object method call return on matching line' do
        expect(subs.replace('bar')).to(eq(['rab']))
      end

      it 'calls effective object method on matching line (other)' do
        subs.replace('foo')
        expect(effective_subs).to(have_received(:foo))
      end

      it 'does not call other effective object method (other)' do
        subs.replace('foo')
        expect(effective_subs).not_to(have_received(:bar))
      end

      it 'returns effective object method call return on matching line (other)' do
        expect(subs.replace('foo')).to(eq(['oof']))
      end
    end
  end
end
